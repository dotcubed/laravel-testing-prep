<?php

namespace Dotcubed\LaravelTestingPrep;

use Dotcubed\LaravelTestingPrep\Commands\TestingPrep;
use Illuminate\Support\ServiceProvider;

class LaravelTestingPrepServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                TestingPrep::class,
            ]);
        }

        $this->publishes(
            [
                __DIR__.'/Config/testing-prep.php' => config_path('testing-prep.php'),
            ],
            'testing-prep',
        );
    }
}
