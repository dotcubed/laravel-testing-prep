<?php

namespace Dotcubed\LaravelTestingPrep\Commands;

use Dotenv\Dotenv;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TestingPrep extends Command
{
    /**
     * The name and signature of the console command.
     * dropAndCreateDb: flag that determines if the testing DB will be dropped/created
     *
     * @var string
     */
    protected $signature = 'testing:prep {--dropAndCreateDb=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets up the testing database so that units tests can be run against it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setVerbosity('quiet');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkEnvExists('.env');
        $this->checkEnvExists('.env.testing');

        if (empty(config('database.connections.' . config('testing-prep.schema.test_database_connection', 'mysql_testing')))) {
            $this->line('The test database connection named >>> ' . config('testing-prep.schema.test_database_connection', 'mysql_testing').' <<< is not present. This means that the schema generated will not be able to load in the test database. Please make sure that the connection exists in the file /config/database.php');
            exit();
        }

        $schemaFilePath = database_path() . DIRECTORY_SEPARATOR . 'schema' . DIRECTORY_SEPARATOR . 'mysql-schema.sql';
        $this->info('Checking for existing schema file on ' . $schemaFilePath);
        $flagSchemaAlreadyExists = file_exists($schemaFilePath);

        if ($flagSchemaAlreadyExists) {
            $this->info('Schema file already exists on ' . $schemaFilePath);
        }

        $this->info('Generating/Updating the DB schema for ' . env('DB_DATABASE'));
        Artisan::call('schema:dump');

        $currentEnvDbName = env('DB_DATABASE');

        $this->loadEnv('.env.testing');

        $testingEnvDbName = env('DB_DATABASE');

        if ($currentEnvDbName == $testingEnvDbName) {
            $this->line('The current DB name and the test DB name are the same. You must keep them separate');
            exit();
        }

        $dropAndCreateDb = $this->option('dropAndCreateDb');
        $this->info('The testing DB "' . env('DB_DATABASE') . '" WILL ' . (! $dropAndCreateDb ? 'NOT ' : '').'be dropped/created.');

        if ($dropAndCreateDb) {

            $this->info('Droping DB '.env('DB_DATABASE'));
            $sql = 'DROP DATABASE IF EXISTS `' . env('DB_DATABASE').'`';
            $this->info('    running: ' . $sql);
            DB::statement($sql);

            $this->info('Creating test DB ' . env('DB_DATABASE'));
            $sql = 'CREATE DATABASE `'.env('DB_DATABASE').'`';
            $this->info('    running: ' . $sql);
            DB::statement($sql);

            $this->info('Reconnecting to DB ' . env('DB_DATABASE'));
            $this->loadEnv('.env.testing');
        }

        $this->info('Loading DB schema dump on DB '.env('DB_DATABASE'));
        DB::unprepared(file_get_contents(database_path() . DIRECTORY_SEPARATOR . 'schema' . DIRECTORY_SEPARATOR . 'mysql-schema.sql'));

        if (! $flagSchemaAlreadyExists) {
            $this->info('Deleting the schema file on ' . $schemaFilePath);
            unlink($schemaFilePath);
        }

        $this->info('Running seeders on DB ' . env('DB_DATABASE'));
        $seeders = config('testing-prep.seeders', ['DatabaseSeeder']);

        foreach ($seeders as $seeder) {
            $this->info('Running seeder ' . $seeder);
            $this->call(
                'db:seed',
                [
                    '--class' => $seeder,
                ]
            );
        }

        $this->line('Finished setting up the test DB ' . env('DB_DATABASE'));
    }

    private function clearLoadedEnvironmentVariables()
    {
        foreach ($_ENV as $key => $value) {
            unset($_ENV[$key], $_SERVER[$key]);
        }
    }

    private function loadEnv($envFile)
    {
        // Specify the path to your custom .env file
        $envPath = base_path();

        if (file_exists($envPath . DIRECTORY_SEPARATOR . $envFile)) {
            $this->info('Loading configuration from ' . $envFile);
            $this->clearLoadedEnvironmentVariables();
            $dotenv = Dotenv::createImmutable($envPath, $envFile);
            $dotenv->load();

            $this->reloadDbFacadeConnection();
        }
    }

    private function checkEnvExists($envFile)
    {
        $this->info('Checking that file ' . $envFile . ' exists');

        if (! file_exists(base_path() . DIRECTORY_SEPARATOR . $envFile)) {
            $this->line('The ' . $envFile . ' file is missing. Please create one based on the ' . $envFile . '.example file and configure it accordingly');
            exit();
        }
    }

    private function reloadDbFacadeConnection()
    {
        $this->info('Reconnecting to target DB ' . env('DB_DATABASE'));

        $defaultConnection = DB::getDefaultConnection();
        Config::set("database.connections.{$defaultConnection}.database", env('DB_DATABASE'));
        DB::purge($defaultConnection);
        DB::reconnect($defaultConnection);
    }
}
