<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Test DB params
    |--------------------------------------------------------------------------
    |
    | Here you will be able to configure what database params will be used when
    | creating the schema, the testing DB and the seeders
    |
    */

    'schema' => [

        /**
         * The database connection to be used by Laravel to create the schema file from
         */
        'database_connection' => 'mysql',

        /**
         * The database connection that the schema will be loaded into
         */
        'test_database_connection' => 'mysql_testing',
    ],

    /*
    |--------------------------------------------------------------------------
    | Seeders
    |--------------------------------------------------------------------------
    |
    | Here you can configure the list of seeders that will run after the
    | test database is loaded
    |
    */

    'seeders' => [
        'DatabaseSeeder',
    ],
];
