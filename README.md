# README #

## What is this repository for? ##

This repo hooks an Artisan command that duplicates a project's database to a testing one so that automated tests can be run against it. 


## How do I get set up? ##

You will need the following already in place:

- a .env file with a working and connectable database
- a .env.testing file pointing to the testing database
- a mysql_testing configuration block in /config/database.php

## Running the Artisan ##

To run the Artisan use the following terminal command:


    php artisan testing-prep
    

## Behind the scenes##

The following happens when running the command:

- **.env** and **.env.testing** files are checked
- mysql_testing configuration block is checked
- a schema:dump is run against the **.env** database
- the **.env.testing database** is dropped, if it exists
- the **.env.testing database** is created and loaded with the schema:dump output
- Seeders are run against the **.env.testing** database (default: **/database/seeders/DatabaseSeeder.php**)

## Configuration ##

To publish the configuration (optional) to the **/config/testing-prep.php** file run the following terminal command:


    php artisan vendor:publish --tag=testing-prep


The following configurable options are available:

|option|type|default|description|
|-----------------------------------|-------------------------------------|-------------------------------------|--------------------------------------------------------------------------------------------------------------|
|schema.database_connection|string|mysql|the current project's database connection name|
|schema.test\_database\_connection|string|mysql_testing)|the test database connection name to be used|
|seeders|array|[DatabaseSeeder]|a list of seeders inside **/database/seeders** that will be run after the testing DB is created and migrated|
